import React from 'react';
import { useApiRepo } from './hooks/useApiRepo';
import { toDoRepository } from './repositories/todoRepository';
import { ResultRenderer } from './components/ResultRenderer';


function App() {
  const result = useApiRepo(toDoRepository.list);
  return (
    <ResultRenderer
      result={result}
      error="Problem fetching todo list. Please try again later."
      success={list => (
        <ul>
          {list.map(todo => <li>{todo.title}</li>)}
        </ul>
      )}
    />
  );
}

export default App;
