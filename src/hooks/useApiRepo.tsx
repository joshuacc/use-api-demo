import { useState, useEffect } from "react";

interface LoadingCase {
  status: 'LOADING';
}

interface SuccessCase<T> {
  status: 'SUCCESS';
  data: T;
}

interface ErrorCase {
  status: 'ERROR';
  error: Error;
}

export type HookResult<T> = LoadingCase | SuccessCase<T> | ErrorCase;

interface UseApiRepoOptions<T> {
  /* An array of arguments to use in calling the repository function */
  args?: T[];
  /* Any additional dependencies which should cause a re-fetch */
  deps?: any[]; // This is one of the rare cases where any is appropriate
}

/**
 * Fetches data from server on component mount, and wraps it in `HookResult` for safer rendering via `ResultRenderer`
 *
 * @param repoFn A repository function which can be called to fetch data from the server.
 * @returns An object containing the status, and possibly the data or error. For safety, the status must be checked before accessing the data or error.
 */
export const useApiRepo = <T, U>(
  repoFn: (...args: U[]) => Promise<T>,
  { args = [], deps = [] }: UseApiRepoOptions<U> = {}
): HookResult<T> => {
  const [result, setResult] = useState<HookResult<T>>({ status: 'LOADING' });

  useEffect(() => {
    let mounted = true;

    repoFn(...args)
      .then(data => {
        if (mounted) {
          setResult({ status: 'SUCCESS', data: data });
        }
      })
      .catch((error: Error) => {
        if (mounted) {
          setResult({ status: 'ERROR', error });
        }
      });

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps This can't be statically verified by eslint.
  }, [repoFn, ...args, ...deps]);

  return result;
};