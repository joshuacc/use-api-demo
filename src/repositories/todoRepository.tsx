import axios from 'axios';

interface ToDo {
  id: string;
  title: string;
  complete: boolean;
}

export const toDoRepository = {
  list():Promise<ToDo[]> {
    return axios.get('http://localhost:3333/todos').then(r => r.data);
  },
  getById(id: string):Promise<ToDo> {
    return axios.get(`http://localhost:3333/todo/${id}`).then(r => r.data);
  }
};