import { HookResult } from "../hooks/useApiRepo";

interface ResultRendererProps<T> {
  /* A result object, usually obtained by calling the `useApiRepo` hook. */
  result: HookResult<T>;
  /* Content to display during loading. If nothing is provided, a default loading message will be displayed. */
  loading?: React.ReactNode;
  /* The content to display in case of an success. It may be JSX, or a render function. */
  success: React.ReactNode | ((data: T) => React.ReactNode);
  /* The content to display in case of an error. It may be JSX, or a render function. */
  error: React.ReactNode | ((error: Error) => React.ReactNode);
}

/**
 * Handles rendering results of async operations that can be in a loading, success or error state.
 * The success and error props can accept JSX directly, or a callback function which accepts the data/error and returns JSX.
 */
export const ResultRenderer = <T extends {}>({
  result,
  loading,
  success,
  error,
}: ResultRendererProps<T>) => {
  switch (result.status) {
    case 'LOADING':
      return loading || 'Loading...'; // This would be a spinner component in a real app
    case 'SUCCESS':
      return typeof success === 'function' ? success(result.data) : success;
    case 'ERROR':
      return typeof error === 'function' ? error(result.error) : error;
  }
};