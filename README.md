# use-api-demo

This is a simplified demonstration of a technique which I use to help ensure that error cases are handled appropriately in React projects written in TypeScript. I recommend opening up a second window to read through the code along with this introduction.

## The problem

When developing a project on powerful desktops with solid internet connections, it is very easy for developers to overlook loading states and possible data loading errors.

## The solution

There are three pieces to consider: The repositor(y|ies), the `useApiRepo` hook, and the `<ResultRenderer />` component. Let's work backwards.

### The `<ResultRenderer />`

Every piece of data fetched from the server should be rendered by the `<ResultRenderer />` component. It's type definition requires the developer to supply both success and error cases, or face the wrath of the compiler. :)
It also supplies a default loading state, with the option of overriding it with something more appropriate.

```tsx
<ResultRenderer
  result={result}
  error="Problem fetching todo list. Please try again later."
  success={list => (
    <ul>
        {list.map(todo => <li>{todo.title}</li>)}
    </ul>
  )}
/>
```

### The `useApiRepo` Hook

In order for `<ResultRenderer />` to know which state to display, it needs to be handed the data in an expected format. This is the `HookResult<T>` type, which can be in a success, error, or loading state.

The `useApiRepo` hook handles all the general data loading logic

```tsx
const result = useApiRepo(toDoRepository.list);
```

Although not used in this demo, there are options for passing arguments and additional dependencies.

```tsx
const result = useApiRepo(toDoRepository.getById, { args: ['MY_ID'], deps: [anotherDependency] });
```

### The Repositories

There is a little niggling problem remaining. We need `<ResultRenderer />` to know the type of the data we are returning. This is where the repositories come in. 

The repositories serve as a centralized place to keep any data loading logic that is particular to an individual type of data: users, todos, etc. The repository knows where to load the data from, and it also knows **the type of the data**.

So when we hand a repository function over to `useApiRepo`, it extracts the type from the repository function, and is able to then pass it along to `<ResultRenderer />`. Then, finally, the type can be given to the `success` render function so that type safety is maintained throughout rendering.

Try using autocomplete on the `list` and `todo` variables in `App.tsx`. If you're using an editor with good TypeScript support, it should "just work." (Assuming you've run `npm install`.)

## Further Notes

In a real application, I implement this via `useReducer` under the hood, which provides a great deal of extensibility via consumer-provided custom reducers. For example, something like this:

```tsx
const [result, dispatch] = useApiRepo(toDoRepository.list, {
  reducers: {
    MARK_COMPLETE() {
      /* Logic omitted */
    }
  }
});

// Later on...
<Button onClick={() => dispatch({ type: 'MARK_COMPLETE', id: 'AN_ID' })}>
```
