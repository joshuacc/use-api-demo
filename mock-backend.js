module.exports = () => {

  const todos = [];

  for (let i = 0; i < 33; i++) {
    todos.push({
      id: i,
      title: `Item ${i}`,
      complete: i % 2 === 0
    });
  }

  return { todos };
}